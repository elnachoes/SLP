extends Control

onready var WinnerLabel : Label = $VBoxContainer/WinnerLabel

func _ready():

	var player = get_tree().get_network_unique_id()

	if (player == NetworkManager.UserList["player1"] and GameMode.GameModeState["IsPlayer1Winner"]):
		WinnerLabel.text = "You Won!"
	else:
		WinnerLabel.text = "You Lost!"
		
	
	if (player == NetworkManager.UserList["player2"] and !GameMode.GameModeState["IsPlayer1Winner"]):
		WinnerLabel.text = "You Won!"
	else:
		WinnerLabel.text = "You Lost!"

func _on_PlayAgainButton_pressed():
	print_debug("%s" % GameMode.GameModeState)
