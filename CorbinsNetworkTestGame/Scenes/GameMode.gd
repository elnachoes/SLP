extends Node

enum GAME_MODES {
	START_SCREEN = 0,
	GAME_SCREEN = 1, 
	GAME_OVER_SCREEN = 2
	}

var GameModeState : Dictionary = {
	"CurrentGameMode" : GAME_MODES.START_SCREEN,
	"GameOver" : false,
	"IsPlayer1Winner" : false,
	"IsTied" : false
} 

# Called when the node enters the scene tree for the first time.
func _ready():
	GameModeState["CurrentGameMode"] = GAME_MODES.START_SCREEN
	
func _process(delta):
	if (GameModeState["CurrentGameMode"] == GAME_MODES.START_SCREEN):
		StateStartScreen()
		
	if (GameModeState["CurrentGameMode"] == GAME_MODES.GAME_SCREEN):
		StateGameScreen()
		
	if (GameModeState["CurrentGameMode"] == GAME_MODES.GAME_OVER_SCREEN):
		StateGameOverScreen()

#####################################
# game mode state machine functions #
#####################################

func TransitionTo_StateStartScreen():
	pass

func StateStartScreen():
	if (NetworkManager.PlayerCount == 2):
		TransitionTo_StateGameScreen()


func TransitionTo_StateGameScreen():
	GameModeState["CurrentGameMode"] = GAME_MODES.GAME_SCREEN
	rpc("SyncGameModeState", GameModeState)
	rpc("LoadGameScreen")
	

func StateGameScreen():
	if (GameModeState["GameOver"]):
		TransitionTo_StateGameOverScreen()
	
func TransitionTo_StateGameOverScreen():
	GameModeState["CurrentGameMode"] = GAME_MODES.GAME_OVER_SCREEN
	rpc("SyncGameModeState", GameModeState)
	rpc("LoadGameOverScreen")

func StateGameOverScreen():
	pass


func SetIsPlayer1Winner(IsPlayer1Winner : bool):
	GameModeState["IsPlayer1Winner"] = IsPlayer1Winner
	GameModeState["GameOver"] = true
	rpc("SyncGameModeState",GameModeState)
	rpc("DeclareResults")

func SetIsTied(IsTied : bool):
	GameModeState["IsTied"] = IsTied
	GameModeState["GameOver"] = true
	rpc("SyncGameModeState",GameModeState)
	rpc("DeclareResults")



#####################
# network functions #
#####################

remotesync func LoadStartScreen():
	get_tree().change_scene("res://Scenes/StartScreen.tscn")

remotesync func LoadGameScreen():
	get_tree().change_scene("res://Scenes/GameScreen.tscn")

remotesync func LoadGameOverScreen():
	get_tree().change_scene("res://Scenes/GameOverScreen.tscn")

remotesync func SyncGameModeState(NewGameModeState : Dictionary):
	GameModeState = NewGameModeState

remotesync func DeclareResults():
	if(GameModeState["IsTied"]):
		print("tie")
		return
	
	if(GameModeState["IsPlayer1Winner"]):
		print("player 1 wins")
	else:
		print("player 2 wins")

