extends Node

####################################
###-------- SERVER STUFF --------###
####################################

signal SuccessfulConnection

# debug port int so i dont have to put it in every time
const DEBUG_PORT : int = 3070

const MAX_PLAYERS : int = 2

var PlayerCount : int = 1

var UserList : Dictionary

func _ready() -> void:
	get_tree().connect("connected_to_server",self,"ConnectedToServer")
	get_tree().connect("server_disconnected",self,"ServerDisconnected")
	get_tree().connect("connection_failed", self, "FailedToConnect")
	get_tree().connect("network_peer_connected", self, "PlayerConnected")

func CreateServer() -> void:
	var Server : NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	Server.create_server(DEBUG_PORT,3)
	get_tree().set_network_peer(Server)
	UpdateUserList(get_tree().get_network_unique_id())
	print("hosting server")

func UpdateUserList(Id):
	var UserName = "player%s" % PlayerCount
	UserList[UserName] = Id
	DisplayUserList()
	rpc("ClientReceiveUserList",UserList)

func ConnectedToServer() -> void:
	pass

func DisconnectedFromServer() -> void:
	print("someone disconnected")

func FailedToConnect() -> void:
	print("failed to connect to server")

func PlayerConnected(Id : int) -> void:
	if (get_tree().get_network_unique_id() == 1):
		PlayerCount += 1
		UpdateUserList(Id)
#	emit_signal("SuccessfulConnection")

####################################
###-------- CLIENT STUFF --------###
####################################

remote func ClientReceiveUserList(NewUserList : Dictionary):
	UserList = NewUserList
	DisplayUserList()

func JoinServer(Ipv4 : String) -> void:
	var Client : NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	Client.create_client(Ipv4,DEBUG_PORT)
	get_tree().set_network_peer(Client)

###########################################
###-------- SERVER/CLIENT STUFF --------###
###########################################

func DisplayUserList():
	print("\nconnected players :")
	for i in UserList:
		print("	Name: %s id: %s" % [i,UserList[i]])
