extends Control

# default gamestate values
var DefaultGameState : Dictionary = {
	"Player1Choice" : "",
	"Player2Choice" : "",
}
# gamestate struct that stores the state of the game
var GameState : Dictionary


################################
# Gamestate managing functions #
################################

remote func SetPlayer1Choice(Choice : String):
	GameState["Player1Choice"] = Choice
	rpc("SyncGamestate",GameState)

remote func SetPlayer2Choice(Choice : String):
	GameState["Player2Choice"] = Choice
	rpc("SyncGamestate",GameState)

remote func SyncGamestate(NewGameState : Dictionary):
	GameState = NewGameState

#############################
# gamescreen main functions #
#############################

func _ready():
	UpdateDisplayHostOrClient()
	GameState = DefaultGameState

func UpdateDisplayHostOrClient():
	var DisplayHostOrClient = $VBoxContainer/DisplayHostOrClient
	if (get_tree().is_network_server()):
		DisplayHostOrClient.text = "Server"
	else:
		DisplayHostOrClient.text = "Client"

remote func DetermineResult():

	if(GameState["Player1Choice"] != "" and GameState["Player2Choice"] != ""):

		if (GameState["Player1Choice"] == GameState["Player2Choice"]):
			GameMode.SetIsTied(true)

		elif (GameState["Player1Choice"] == "paper" and GameState["Player2Choice"] == "rock"):
			GameMode.SetIsPlayer1Winner(true)

		elif (GameState["Player1Choice"] == "rock" and GameState["Player2Choice"] == "scissors"):
			GameMode.SetIsPlayer1Winner(true)

		elif (GameState["Player1Choice"] == "scissors" and GameState["Player2Choice"] == "paper"):
			GameMode.SetIsPlayer1Winner(true)

		else:
			GameMode.SetIsPlayer1Winner(false)


################################
# button press signal handlers #
################################


func _on_PaperButton_pressed():
	var player = get_tree().get_network_unique_id()

	if (player == NetworkManager.UserList["player1"] and get_tree().is_network_server()):
		SetPlayer1Choice("paper")
		print("server and player 1 picking paper")
	else:
		rpc_id(1, "SetPlayer2Choice", "paper")

	if (get_tree().is_network_server()):
		DetermineResult()
	else:
		rpc_id(1, "DetermineResult")

func _on_RockButton_pressed():
	var player = get_tree().get_network_unique_id()

	if (player == NetworkManager.UserList["player1"] and get_tree().is_network_server()):
		SetPlayer1Choice("rock")
		print("server and player 1 picking rock")
	else:
		rpc_id(1, "SetPlayer2Choice", "rock")

	if (get_tree().is_network_server()):
		DetermineResult()
	else:
		rpc_id(1, "DetermineResult")

func _on_ScissorsButton_pressed():
	var player = get_tree().get_network_unique_id()

	if (player == NetworkManager.UserList["player1"] and get_tree().is_network_server()):
		SetPlayer1Choice("scissors")
#		print("server and player 1 picking scissors")
	else:
		rpc_id(1, "SetPlayer2Choice", "scissors")

	if (get_tree().is_network_server()):
		DetermineResult()
	else:
		rpc_id(1, "DetermineResult")



#################################################################################

