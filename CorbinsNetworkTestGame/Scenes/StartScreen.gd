extends Control

#uncomment this when we are ready to host on stuff other than our own machine
#onready var IPV4AddressField : LineEdit = $VBoxContainer/HBoxContainer2/IPV4AddressField

# debug ipv4 string so i dont have to put it in every time 
const DEBUG_IPV4 : String = "127.0.0.1"

func _ready() -> void:
	pass

func _on_HostButton_pressed() -> void:
	NetworkManager.CreateServer()

func _on_JoinButton_pressed() -> void:
	NetworkManager.JoinServer(DEBUG_IPV4)

